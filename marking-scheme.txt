Total mark: [100/100]
---------------------------------------------------

Client properly initiates contact with the aserver [10/100]
Client computes a valid secret and communicates it to the aserver [40/100]
Client properly communicates to the fserver and provides the right fnonce [25/100]
Client prints out the right fortune message [25/100]

** Congratulations, your solution was one of the 10 fastest in the
class. You will have 2% added to your final course grade. **

Note: grade updated
