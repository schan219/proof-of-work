/*
Implements the solution to assignment 1 for UBC CS 416 2017 W2.

Usage:
$ go run client.go [local UDP ip:port] [local TCP ip:port] [aserver UDP ip:port]

Example:
$ go run client.go 127.0.0.1:2020 127.0.0.1:3030 127.0.0.1:7070

*/

package main

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"

	// TODO
	"os"
	"fmt"
	"net"
	"strconv"
	"math/rand"
)

/////////// Msgs used by both auth and fortune servers:

// An error message from the server.
type ErrMessage struct {
	Error string
}

/////////// Auth server msgs:

// Message containing a nonce from auth-server.
type NonceMessage struct {
	Nonce string
	N     int64 // PoW difficulty: number of zeroes expected at end of md5(nonce+secret)
}

// Message containing an the secret value from client to auth-server.
type SecretMessage struct {
	Secret string
}

// Message with details for contacting the fortune-server.
type FortuneInfoMessage struct {
	FortuneServer string // TCP ip:port for contacting the fserver
	FortuneNonce  int64
}

/////////// Fortune server msgs:

// Message requesting a fortune from the fortune-server.
type FortuneReqMessage struct {
	FortuneNonce int64
}

// Response from the fortune-server containing the fortune.
type FortuneMessage struct {
	Fortune string
	Rank    int64 // Rank of this client solution
}


/////////// Helper functions:

func handleError(err error) {
	if err != nil {
		fmt.Println("Error: ", err)
		os.Exit(-1)
	}
}

func handleErrorFromServer(buf []byte) bool {
	var errMessage ErrMessage
	err := json.Unmarshal(buf, &errMessage)
	handleError(err)

	fmt.Println("Error from server: ", errMessage.Error)
	return true
}

func connectToAserver(localUDP string, aserverUDP string) *net.UDPConn {
	ServerAddr, err := net.ResolveUDPAddr("udp", aserverUDP)
	handleError(err)
	LocalAddr, err := net.ResolveUDPAddr("udp", localUDP)
	handleError(err)
	Conn, err := net.DialUDP("udp", LocalAddr, ServerAddr)
	handleError(err)

	return Conn
}

func connectToFserver(localTCP string, fserverTCP string) (*net.TCPConn, *net.TCPAddr) {
	ServerAddr, err := net.ResolveTCPAddr("tcp", fserverTCP)
	handleError(err)
	LocalAddr, err := net.ResolveTCPAddr("tcp", localTCP)
	Conn, err := net.DialTCP("tcp", LocalAddr, ServerAddr)
	handleError(err)
	return Conn, ServerAddr
}

func sendToAserver(Conn *net.UDPConn, content []byte) {
	_, err := Conn.Write(content)
	handleError(err)
}

func receiveFromAserver(Conn *net.UDPConn) ([]byte, int) {
	buf := make([]byte, 1024)
	bufLen, err := Conn.Read(buf)
	handleError(err)
	return buf, bufLen
}

func sendToFserver(Conn *net.TCPConn, ServerAddr *net.TCPAddr, content []byte) {
	_, err := Conn.Write(content)
	handleError(err)
}

func receiveFromFserver(Conn *net.TCPConn) ([]byte, int) {
	buf := make([]byte, 1024)
	bufLen, err := Conn.Read(buf)
	handleError(err)
	return buf, bufLen
}

func findHash(nonceMessage NonceMessage, nZeroes string, c chan string) {
	var secret string
	var hash string
	for {
		secret = strconv.Itoa(rand.Intn(100000000000000))
		hash = computeNonceSecretHash(nonceMessage.Nonce, secret)
		lastDigits := hash[int64(len(hash))-nonceMessage.N:]
		if lastDigits == nZeroes {
			c <- secret
			break
		}
	}
}

func findSecret(localUDP string, aserverUDP string, buf []byte) FortuneInfoMessage {
	var ConnA *net.UDPConn
	var arbitrary string
	var secret string

	// Connect to aserver
	ConnA = connectToAserver(localUDP, aserverUDP)

	arbitrary = "Meanie Blue"
	marshalledSecret, err := json.Marshal(arbitrary)
	handleError(err)

	sendToAserver(ConnA, marshalledSecret)

	buf, bufLen := receiveFromAserver(ConnA)

	var nonceMessage NonceMessage
	err = json.Unmarshal(buf[:bufLen], &nonceMessage)
	handleError(err)

	nZeroes := "0"
	for i := int64(1); i<nonceMessage.N; i++ {
		nZeroes += "0"
	}

	c := make(chan string)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	go findHash(nonceMessage, nZeroes, c)
	secret = <-c

	secretMessage := SecretMessage{secret}
	marshalledSecret, err = json.Marshal(secretMessage)
	handleError(err)

	sendToAserver(ConnA, marshalledSecret)
	buf, bufLen = receiveFromAserver(ConnA)
	ConnA.Close()

	var fortuneInfoMessage FortuneInfoMessage
	err = json.Unmarshal(buf[:bufLen], &fortuneInfoMessage)
	handleError(err)

	return fortuneInfoMessage
}

// go run client.go 198.162.33.28:18000 198.162.33.28:18060 198.162.33.54:5555
// 142.103.15.6:7777
// Main workhorse method.
func main() {
	// TODO
	// Use json.Marshal json.Unmarshal for encoding/decoding to servers
	args := os.Args

	if len(args) != 4 {
		fmt.Println("Usage: go run client.go [local UDP ip:port] [local TCP ip:port] [server UDP ip:port]")
		return
	}

	var localUDP = args[1]
	var localTCP = args[2]
	var aserverUDP = args[3]
	var ConnF *net.TCPConn
	var ServerAddrF *net.TCPAddr
	var buf = make([]byte, 1024)

	var fortuneInfoMessage = findSecret(localUDP, aserverUDP, buf)

	fortuneReqMessage := FortuneReqMessage{fortuneInfoMessage.FortuneNonce}
	marshalledFortuneReq, err := json.Marshal(fortuneReqMessage)
	handleError(err)

	// Open connection to fserver.
	ConnF, ServerAddrF = connectToFserver(localTCP, fortuneInfoMessage.FortuneServer)
	// Send fortune request message
	sendToFserver(ConnF, ServerAddrF, marshalledFortuneReq)
	buf, bufLen := receiveFromFserver(ConnF)

	var fortuneMessage FortuneMessage
	err = json.Unmarshal(buf[:bufLen], &fortuneMessage)
	handleError(err)

	fmt.Println(fortuneMessage.Fortune)
}

// Returns the MD5 hash as a hex string for the (nonce + secret) value.
func computeNonceSecretHash(nonce string, secret string) string {
	h := md5.New()
	h.Write([]byte(nonce + secret))
	str := hex.EncodeToString(h.Sum(nil))
	return str
}
